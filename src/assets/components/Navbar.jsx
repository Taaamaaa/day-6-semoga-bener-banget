import React, { useState } from "react";
import CameraAltIcon from "@mui/icons-material/CameraAlt";
import MenuIcon from "@mui/icons-material/Menu";
import "../css/Navbar.css";
const Navbar = () => {
  const [showMobileMenu, setShowMobileMenu] = useState(false);

  const toggleMobileMenu = () => {
    setShowMobileMenu(!showMobileMenu);
  };

  return (
    <div className="navbar">
      <div className="navbar-left">
        <CameraAltIcon />
        <span className="navbar-title">Blog App</span>
        <span className="navbar-title">About</span>
        <span className="navbar-title">Memory</span>
      </div>
      <div className="navbar-right">
        <button className="navbar-button">Login</button>
      </div>
      <div className="navbar-mobile">
        <div className="navbar-mobile-left">
          <CameraAltIcon />
        </div>

        <MenuIcon onClick={toggleMobileMenu} />

        {showMobileMenu && (
          <div className="navbar-mobile-menu">
            <button className="navbar-button">Login</button>
            <span className="navbar-title">Blog App</span>
            <span className="navbar-title">About</span>
            <span className="navbar-title">Memory</span>
          </div>
        )}
      </div>
    </div>
  );
};

export default Navbar;
