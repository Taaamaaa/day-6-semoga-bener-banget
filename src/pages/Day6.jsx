import React, { useEffect, useState } from "react";
import axios from "axios";
import Navbar from "../assets/components/Navbar";

function LayoutPage() {
  const [postData, setPostData] = useState([]);
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const fetchPostData = async () => {
      try {
        const response = await axios.get("http://localhost:3004/postgenerated");
        setPostData(response.data);
      } catch (error) {
        console.error("Error fetching post data:", error);
      }
    };

    fetchPostData();

    const handleResize = () => {
      setIsMobile(window.innerWidth <= 600);
    };

    handleResize();
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div>
      <Navbar />
      <div
        style={{
          maxWidth: "1200px",
          margin: "0 auto",
          backgroundColor: "#f9f9f9",
          padding: "20px",
          boxSizing: "border-box",
          fontFamily: "Arial, sans-serif",
        }}
      >
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
          }}
        >
          <div
            className="content"
            style={{
              flex: "2",
              backgroundColor: "#ffffff",
              padding: "20px",
              boxSizing: "border-box",
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {postData.map((post, index) => (
              <div
                key={post.post_id}
                className="card"
                style={{
                  flex: isMobile ? "0 0 100%" : "0 0 calc(50% - 10px)",
                  backgroundColor: "#f7f7f7",
                  border: "1px solid #e5e5e5",
                  borderRadius: "5px",
                  marginBottom: "20px",
                  padding: "20px",
                  boxSizing: "border-box",
                  width: "100%",
                  maxWidth: isMobile ? "100%" : "calc(50% - 10px)",
                  marginRight: isMobile || index % 2 === 1 ? "0" : "20px",
                }}
              >
                <h3
                  style={{
                    marginBottom: "10px",
                    fontSize: "20px",
                    fontWeight: "bold",
                    color: "#333333",
                  }}
                >
                  {post.title}
                </h3>
                <img
                  src={post.img}
                  alt="Post Image"
                  style={{
                    width: "100%",
                    height: "auto",
                    marginBottom: "10px",
                  }}
                />
                <p
                  style={{
                    fontSize: "16px",
                    fontWeight: "normal",
                    marginBottom: "5px",
                    color: "#555555",
                  }}
                >
                  Author: {post.author} {post.lastName}
                </p>
                <p
                  style={{
                    fontSize: "16px",
                    fontWeight: "normal",
                    marginBottom: "5px",
                    color: "#555555",
                  }}
                >
                  Date Posted: {post.datePost}
                </p>
                <p
                  style={{
                    fontSize: "16px",
                    fontWeight: "normal",
                    marginBottom: "0",
                    color: "#555555",
                  }}
                >
                  {post.description}
                </p>
              </div>
            ))}
          </div>
          {!isMobile && (
            <div
              className="sidebar"
              style={{
                flex: "1",
                backgroundColor: "#9c9c9c",
                color: "#ffffff",
                padding: "20px",
                boxSizing: "border-box",
              }}
            >
              <h2>Sidebar</h2>
              <p>This is the sidebar content.</p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default LayoutPage;
